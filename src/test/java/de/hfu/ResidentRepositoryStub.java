package de.hfu;

import de.hfu.integration.domain.Resident;
import de.hfu.integration.repository.ResidentRepository;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ResidentRepositoryStub implements ResidentRepository {

    @Override
    public List<Resident> getResidents() {
        List<Resident> residentList = new ArrayList<>();

        residentList.add(new Resident("David", "Pospisil1", "Bahnhofstrasse1", "Furtwangen", getDate("2004-07-29")));
        residentList.add(new Resident("David", "Pospisil1", "Bahnhofstrasse1", "Furtwangen", getDate("2004-07-29")));
        residentList.add(new Resident("Jonas", "Pospisil2", "Bahnhofstrasse2", "Furtwangen", getDate("2004-07-29")));
        residentList.add(new Resident("Sarah", "Pospisil3", "Bahnhofstrasse3", "Furtwangen", getDate("2004-07-29")));
        residentList.add(new Resident("Daniel", "Pospisil4", "Bahnhofstrasse4", "Furtwangen", getDate("2004-07-29")));
        residentList.add(new Resident("Johannes", "Pospisil5", "Bahnhofstrasse5", "Furtwangen", getDate("2004-07-29")));
        residentList.add(new Resident("Fabian", "Pospisil6", "Bahnhofstrasse6", "Furtwangen", getDate("2004-07-29")));
        return residentList;
    }

    private Date getDate(String strDate){
        Date date = null;
        String filterDate = strDate;
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        try{
            date = dateFormatter.parse(filterDate);
        } catch (ParseException ex){ System.out.println("Error"); }
        return date;
    }
}
